<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $created_date;

    /**
     * @var OrderItem[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="order")
     */
    private $order_items;

    /**
     * Order constructor.
     */
    public function __construct()
    {
        $this->order_items = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @param int $number
     * @return $this
     */
    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate(): ?string
    {
        return $this->created_date->format('Y-m-d H:i:s');
    }

    /**
     * @param DateTime $created_date
     * @return $this
     */
    public function setCreatedDate(DateTime $created_date): self
    {
        $this->created_date = $created_date;

        return $this;
    }

    /**
     * @return OrderItem[]|ArrayCollection
     */
    public function getOrderItems()
    {
        return $this->order_items;
    }

    /**
     * @param OrderItem $order_item
     * @return self
     */
    public function addOrderItem(OrderItem $order_item) : self
    {
        if (!$this->order_items->contains($order_item)) {
            $this->order_items[] = $order_item;
            $order_item->setOrder($this);
        }

        return $this;
    }

//    public function setOrderItem(OrderItem $order_item = null)
//    {
//        $this->order_items = $order_item;
//
//        return $this;
//    }

    /**
     * @param OrderItem $order_item
     * @return self
     */
    public function removeOrderItem(OrderItem $order_item) : self
    {
        $this->order_items->removeElement($order_item);

        return $this;
    }

    public function sumItems()
    {
        $sum_price = 0;
        foreach ($this->getOrderItems() as $order_item){
            $sum_price += $order_item->getProductPrice();
        }
        return $sum_price;
    }

//    /**
//     * @ORM\PrePersist()
//     */
//    public function prePersist()
//    {
//        $this->created_date = new \DateTime();
//    }
}
