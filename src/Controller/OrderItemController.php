<?php

namespace App\Controller;

use App\Entity\OrderItem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderItemController extends AbstractController
{
    /**
     * @Route("/order/item", name="order_item")
     */
    public function index(): Response
    {
        $products = $this->getFormattedProducts(new \DateTime('2018-01-01 12:00:00'), new \DateTime('now'));

        return $this->render('order_item/index.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @route("itemFilter", name="itemFilter")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function searchForm(Request $request): Response
    {
        $form = $request->request->get('form');

        $products = $this->getFormattedProducts(new \DateTime($form['from']), new \DateTime($form['to']));

        return $this->render('order_item/index.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @return Response
     */
    public function filterByDate(): Response
    {
        $form = $this->createFormBuilder(null)
            ->setAction($this->generateUrl('itemFilter'))
            ->add('from', DateTimeType::class, [
                'widget' => 'single_text',
            ])
            ->add('to', DateTimeType::class, [
                'widget' => 'single_text',
            ])
            ->add('search', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ])
            ->getForm();

        return $this->render('order_item/searchBar.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return array
     */
    private function getFormattedProducts(\DateTime $from, \DateTime $to): array
    {
        $order_items = $this
            ->getDoctrine()
            ->getRepository(OrderItem::class)
            ->findByAmountDesc($from, $to);

        $products = [];
        foreach ($order_items as $order_item) {
            $products[$order_item->getProductName()][] = $order_item;
        }

        return $products;
    }
}
