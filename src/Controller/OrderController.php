<?php

namespace App\Controller;

use App\Entity\Order;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    /**
     * @Route("/order", name="order")
     */
    public function index(): Response
    {
        $orders = $this
            ->getDoctrine()
            ->getRepository(Order::class)
            ->findAll();

        return $this->render('order/index.html.twig', [
            'orders' => $orders,
        ]);
    }

    /**
     * @return Response
     */
    public function filterByDate(): Response
    {
        $form = $this
            ->createFormBuilder(null)
            ->setAction($this->generateUrl('handleFilter'))
            ->add('from', DateTimeType::class, [
                'widget' => 'single_text',
            ])
            ->add('to', DateTimeType::class, [
                'widget' => 'single_text',
            ])
            ->add('search', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ])
            ->getForm();

        return $this->render('order/searchBar.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @route("/handleFilter", name="handleFilter")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function handleForm(Request $request): Response
    {
        $form = $request->request->get('form');

        $orders = $this
            ->getDoctrine()
            ->getRepository(Order::class)
            ->findByDateTimeInterval(new \DateTime($form['from']), new \DateTime($form['to']));

        return $this->render('order/index.html.twig', [
            'orders' => $orders,
        ]);
    }
}
