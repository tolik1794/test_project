<?php

namespace App\Command;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SeedCommand extends Command
{
    protected static $defaultName = 'seed:order';
    private $manager;
    private $order_repository;

    /**
     * SeedCommand constructor.
     * @param EntityManagerInterface $manager
     * @param OrderRepository $repository
     * @param string|null $name
     */
    public function __construct(EntityManagerInterface $manager, OrderRepository $repository, string $name = null)
    {
        parent::__construct($name);

        $this->manager = $manager;
        $this->order_repository = $repository;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('number', InputArgument::OPTIONAL, 'Set count creating orders!')
            ->addOption('number', 2, InputOption::VALUE_NONE, 'Option description');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $number = $input->getArgument('number');
        $last_id = $this->last_id();

        $manager = 0;
        for ($i = 1; $i <= $number; $i++) {
            $manager = $this->createOrder($last_id + $i);
        }
        $manager->flush();

        $io->success("Create {$number} order" . ($number == 1 ? '' : 's') . " success!");

        return 0;
    }

    /**
     * @param int $count_order
     * @return ObjectManager
     * @throws \Exception
     */
    public function createOrder(int $count_order): ObjectManager
    {
        $manager = $this->manager;

        $date = new \DateTime('2018-01-01 09:00:00');
        $date->add(new \DateInterval("PT{$count_order}H"));

        $order = new Order();
        $order->setCreatedDate($date);

        for ($i = 1; $i <= rand(1, 5); $i++) {
            $order_item = $this->createOrderItem($i);
            $order->addOrderItem($order_item);
            $manager->persist($order_item);
        }

        $manager->persist($order);

        return $manager;
    }

    /**
     * @param int $element
     * @return OrderItem
     */
    public function createOrderItem(int $element): OrderItem
    {
        $order_item = new OrderItem();
        $order_item->setAmount(rand(1, 10));
        $order_item->setProductName('Товар'.$element);
        $order_item->setProductPrice(rand(100, 9999));

        return $order_item;
    }

    /**
     * @return int
     */
    public function last_id(): ?int
    {
        $repository = $this->order_repository;

        $last_order = $repository->findOneBy([], ['id' => 'DESC']);

        return $last_order ? $last_order->getId() : 0;
    }
}
