<?php

namespace App\DataFixtures;

use App\Entity\Order;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $order = new Order();
        $order->setCreatedDate(new \DateTime());

        $manager->persist($order);

        $manager->flush();
    }
}
