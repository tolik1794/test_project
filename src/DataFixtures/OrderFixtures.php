<?php


namespace App\DataFixtures;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Repository\OrderItemRepository;
use App\Repository\OrderRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

//use Doctrine\ORM\EntityManager as ObjectManager;

class OrderFixtures extends Fixture
{
    private $order_repository;
    private $order_item_repository;

    public function __construct(OrderRepository $order_repository, OrderItemRepository $order_item_repository)
    {
        $this->order_repository = $order_repository;
        $this->order_item_repository = $order_item_repository;
    }

    /**
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {
    }
}