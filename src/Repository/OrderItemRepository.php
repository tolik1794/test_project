<?php

namespace App\Repository;

use App\Entity\OrderItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderItem[]    findAll()
 * @method OrderItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderItemRepository extends ServiceEntityRepository
{
    /**
     * OrderItemRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderItem::class);
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return OrderItem[] Returns an array of OrderItem objects
     */
    public function findByAmountDesc(\DateTime $from, \DateTime $to): array
    {
        return $this
            ->createQueryBuilder('o')
            ->addSelect('SUM(o.amount) AS HIDDEN popular')
            ->leftJoin('o.order', 'i')
            ->andWhere('i.created_date >= :from')
            ->setParameter('from', $from)
            ->andWhere('i.created_date <= :to')
            ->setParameter('to', $to)
            ->orderBy('popular', 'DESC')
            ->groupBy('o.id')
            ->getQuery()
            ->getResult();
    }
}
