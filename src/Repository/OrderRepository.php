<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    /**
     * OrderRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return Order[]
     */
    public function findByDateTimeInterval(\DateTime $from, \DateTime $to): array
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.created_date >= :from')
            ->setParameter('from', $from)
            ->andWhere('o.created_date <= :to')
            ->setParameter('to', $to)
            ->getQuery()
            ->getResult();
    }
}
